import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app.routing';
import { UserGuard } from './services/route-guards/user.guard';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ClassesComponent } from './pages/classes/classes.component';
import { ClassDetailComponent } from './pages/classes/class-detail/class-detail.component';
import { ClassListComponent } from './pages/classes/class-list/class-list.component';
import { ClassPlaceholderComponent } from './pages/classes/class-placeholder/class-placeholder.component';
import { ProfessorComponent } from './pages/professor/professor.component';

import { NavbarComponent } from './pages/shared/navbar/navbar.component';

import { ClassService } from './services/api/class.service';
import { HttpService } from './services/utils/http.service';
import { LocalStorage } from './services/utils/local-storage.service';
import { ProfessorService } from './services/api/professor.service';
import { SessionService } from './services/api/session.service';
import { UserService } from './services/api/user.service';
import { UIService } from './services/utils/ui.service';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ClassesComponent,
    ClassDetailComponent,
    ClassListComponent,
    ClassPlaceholderComponent,
    ProfessorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ClassService,
    HttpService,
    LocalStorage,
    ProfessorService,
    SessionService,
    UserService,
    UIService,

    UserGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
