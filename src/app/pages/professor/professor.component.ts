import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SessionService } from '../../services/api/session.service';
import { ProfessorService } from '../../services/api/professor.service';
import { UIService } from '../../services/utils/ui.service';

@Component({
  selector: 'app-professor',
  templateUrl: './professor.component.pug'
})
export class ProfessorComponent {
  professor: any;
  ratings: any[];
  classes: any;

  constructor(
    private sessionService: SessionService, 
    private professorService: ProfessorService,
    private uiService: UIService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.requestDetailStart();
      this.requestCoursesStart();
    });
  }

  current_user() {
    return this.sessionService.getCurrentUser();
  }

  averageRating() {
    const average = this.ratings
                          .map(rating => rating.score)
                          .reduce((sum, current) => sum + current, 0) / this.ratings.length;

    return isNaN(average) ? 'No rating yet.' : average;
  }

  getRating(obj: any) {
    const ratings = this.ratings
                      .filter(rating => rating.course_id === obj.id)

    const average = ratings
                          .map(rating => rating.score)
                          .reduce((sum, current) => sum + current, 0) / ratings.length;

    return isNaN(average) ? 'No rating yet.' : average;
  }

  getRatingColor(rate) {
    return this.uiService.ratingTextColor(rate);
  }

  // API request related
  requestDetailStart() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.professorService.getDetail(id).subscribe(resp => {
      this.requestDetailComplete(resp);
    }, err => {
      console.log('err from requestDetailStart')
    })
  }

  requestDetailComplete(resp: any) {
    this.professor = resp.json();
  }

  requestCoursesStart() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.professorService.getCourses(id).subscribe(resp => {
      this.requestCoursesComplete(resp);
    }, err => {
      console.log('err from requestCoursesStart')
    })
  }

  requestCoursesComplete(resp: any) {
    const response = resp.json()
    this.ratings = response.ratings;
    this.classes = response.classes;
  }

}