import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SessionService } from '../../../services/api/session.service';
import { ClassService } from '../../../services/api/class.service';

@Component({
  selector: 'app-class-list',
  templateUrl: './class-list.component.pug'
})
export class ClassListComponent {
  classes: any;

  constructor(
    private sessionService: SessionService, 
    private classService: ClassService,
    private router: Router,
    private route: ActivatedRoute) {
   }

   ngOnInit() {
     this.requestStart();
   }

  requestStart() {
    this.classService.get().subscribe(resp => {
      this.requestComplete(resp);
    }, err => {
      console.log('err from requestStart')
    })
  }

  requestComplete(resp: any) {
    const response = resp.json()
    this.classes = response.collection
  }
}