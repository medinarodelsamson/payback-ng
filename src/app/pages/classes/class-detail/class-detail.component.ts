import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SessionService } from '../../../services/api/session.service';
import { ClassService } from '../../../services/api/class.service';
import { UIService } from '../../../services/utils/ui.service';

@Component({
  selector: 'app-class-detail',
  templateUrl: './class-detail.component.pug'
})
export class ClassDetailComponent {
  professors: any;
  ratings: any;
  course: any;

  constructor(
    private sessionService: SessionService, 
    private classService: ClassService,
    private uiService: UIService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.requestDetailStart();
      this.requestProfessorStart();
    });
  }

  current_user() {
    return this.sessionService.getCurrentUser();
  }

  getRating(obj: any) {
    const ratings = this.ratings
                      .filter(rating => rating.professor_id === obj.id)

    const average = ratings
                          .map(rating => rating.score)
                          .reduce((sum, current) => sum + current, 0) / ratings.length;

    return isNaN(average) ? 'No rating yet.' : average;
  }

  hasRated(obj: any) {
    const ratings = this.ratings
                      .filter(rating => rating.professor_id === obj.id && rating.student_id === this.current_user().id )
    return ratings.length > 0
  }

  toggleRating(score, obj: any) {
    if (obj.score === score) {
      obj.score = null
    } else {
      obj.score = score
    }
  }

  updateComment(event, obj: any) {
    obj.comment = event.target.value;
  }

  activeStar(score, obj: any) {
    if (obj.score === score) {
      return 'btn btn-xs btn-primary text-white ' + this.uiService.ratingButtonColor(score);
    } else {
      return 'btn btn-xs';
    }
  }

  getRatingColor(rate) {
    return this.uiService.ratingTextColor(rate);
  }

  saveRating(obj: any) {
    this.requestSaveRatingStart(obj);
  }

  // API request related
  requestDetailStart() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.classService.getDetail(id).subscribe(resp => {
      this.requestDetailComplete(resp);
    }, err => {
      console.log('error on requestDetailStart')
    })
  }

  requestDetailComplete(resp: any) {
    this.course = resp.json();
  }

  requestProfessorStart() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.classService.getProfessors(id).subscribe(resp => {
      this.requestProfessorComplete(resp);
    }, err => {
      console.log('error on requestProfessorStart')
    })
  }

  requestProfessorComplete(resp: any) {
    const response = resp.json()
    this.ratings = response.ratings;
    this.professors = response.professors;
  }

  requestSaveRatingStart(obj: any) {
    const id = +this.route.snapshot.paramMap.get('id');
    this.classService.saveRating(id, obj.id, {score: obj.score, comment: obj.comment}).subscribe(resp => {
      this.requestSaveRatingComplete(obj, resp);
    }, err => {
      console.log('error on requestSaveRatingStart')
    })
  }

  requestSaveRatingComplete(prof, resp: any) {
    prof.score = null;
    this.ratings.push(resp.json());
  }

}