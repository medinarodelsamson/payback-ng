import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ClassesComponent } from './pages/classes/classes.component';
import { ClassDetailComponent } from './pages/classes/class-detail/class-detail.component';
import { ClassPlaceholderComponent } from './pages/classes/class-placeholder/class-placeholder.component';
import { ProfessorComponent } from './pages/professor/professor.component';

import { UserGuard } from './services/route-guards/user.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'professor/:id', component: ProfessorComponent, canActivate: [UserGuard] },
  { 
    path: 'classes', 
    component: ClassesComponent,
    canActivate: [UserGuard],
    children: [
      { 
        path: '', 
        component: ClassPlaceholderComponent,
      },
      { 
        path: ':id', 
        component: ClassDetailComponent,
      },
    ]
  }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule {}