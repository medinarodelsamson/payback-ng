import { Injectable } from '@angular/core';

@Injectable()
export class UIService {

  constructor() {}

  public ratingTextColor(rate): string {
    return [
      'text-muted',
      'text-danger',
      'text-secondary',
      'text-warning',
      'text-info',
      'text-success',
    ][Math.round(rate)];
  }

  public ratingButtonColor(rate): string {
    return [
      'btn-link',
      'btn-danger',
      'btn-secondary',
      'btn-warning',
      'btn-info',
      'btn-success',
    ][Math.round(rate)];
  }
}
