import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { SessionService } from '../api/session.service';

@Injectable()
export class UserGuard implements CanActivate {
  user: Object;
  constructor(
    private sessionService: SessionService,
    private router: Router,
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.sessionService.isValid()) {
      console.log('logged in');
      return true;
    } else {
      console.log('Not logged in. After login, redirect to:', state.url);
      this.router.navigate(['/login'], {
        queryParams: {
          return: state.url
        }
      });
      return false;
    }
  }
}
