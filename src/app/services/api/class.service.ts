import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';

import { Observable, Subject } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { HttpService } from '../utils/http.service';

const ENDPOINT = `${environment.API_URL}/classes`;

@Injectable()
export class ClassService {
 
  constructor(
    public httpService: HttpService,
    public router: Router,
    private http: Http) {

  }

  get(): any {

    const headers = new Headers();
    this.httpService.createAuthorizationHeader(headers,false);

    return this
            .http
            .get(`${ENDPOINT}`, { headers: headers })
            .pipe(
              map(res => {
                          return res;
              })
            );
  }

  getProfessors(id): any {

    const headers = new Headers();
    this.httpService.createAuthorizationHeader(headers,false);

    return this
            .http
            .get(`${ENDPOINT}/${id}/professors`, { headers: headers })
            .pipe(
              map(res => {
                          return res;
              })
            );
  }

  getDetail(id): any {

    const headers = new Headers();
    this.httpService.createAuthorizationHeader(headers,false);

    return this
            .http
            .get(`${ENDPOINT}/${id}`, { headers: headers })
            .pipe(
              map(res => {
                          return res;
              })
            );
  }

  saveRating(course_id, professor_id, payload: any): any {

    const headers = new Headers();
    this.httpService.createAuthorizationHeader(headers,false);

    return this
            .http
            .post(`${ENDPOINT}/${course_id}/professors/${professor_id}/rate`, { rating: payload }, {headers: headers})
            .pipe(
              map(res => {
                          return res;
                        })
             );
  }
}
