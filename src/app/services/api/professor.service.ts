import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';

import { Observable, Subject } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { HttpService } from '../utils/http.service';

const ENDPOINT = `${environment.API_URL}/professors`;

@Injectable()
export class ProfessorService {
 
  constructor(
    public httpService: HttpService,
    public router: Router,
    private http: Http) {

  }

  getDetail(id): any {

    const headers = new Headers();
    this.httpService.createAuthorizationHeader(headers,false);

    return this
            .http
            .get(`${ENDPOINT}/${id}`, { headers: headers })
            .pipe(
              map(res => {
                          return res;
              })
            );
  }

  getCourses(id): any {

    const headers = new Headers();
    this.httpService.createAuthorizationHeader(headers,false);

    return this
            .http
            .get(`${ENDPOINT}/${id}/classes`, { headers: headers })
            .pipe(
              map(res => {
                          return res;
              })
            );
  }
}
